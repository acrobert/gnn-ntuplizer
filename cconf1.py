from CRABClient.UserUtilities import config#, getUsernameFromSiteDB                                                                                                                                      
 
config = config()

# To submit to crab:                                                         
# crab submit -c crabConfig_data.py                                          
# To check job status:                                                       
# crab status -d <config.General.workArea>/<config.General.requestName>      
# To resubmit jobs:                                                          
# crab resubmit -d <config.General.workArea>/<config.General.requestName>    

# Local job directory will be created in:
# <config.General.workArea>/<config.General.requestName>

date = 'July2022'
config.General.requestName = 'pfml_'+date+'_ntup_gj3'
config.General.workArea = 'crab_run'
config.General.transferOutputs = True
config.General.transferLogs = True

# CMS cfg file goes here:         
config.JobType.psetName = 'PhotonClassifier/RecHitAnalyzer/python/RecHitAnalyzer_cfg.py'
config.JobType.pluginName = 'Analysis'
#config.JobType.maxJobRuntimeMin = 3000
config.JobType.maxMemoryMB = 3072

# Define units per job here:          
#config.Data.inputDataset = '/GJet_Pt-20to40_DoubleEMEnriched_MGG-80toInf_TuneCP5_13TeV_Pythia8/RunIIFall17MiniAODv2-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/MINIAODSIM'
#config.Data.inputDataset = '/GJet_Pt-40toInf_DoubleEMEnriched_MGG-80toInf_TuneCP5_13TeV_Pythia8/RunIIFall17MiniAODv2-PU2017_12Apr2018_94X_mc2017_realistic_v14-v2/MINIAODSIM'
config.Data.inputDataset = '/GJet_Pt-20toInf_DoubleEMEnriched_MGG-40to80_TuneCP5_13TeV_Pythia8/RunIIFall17MiniAODv2-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/MINIAODSIM'
#config.Data.inputDataset = '/DiPhotonJets_MGG-80toInf_13TeV_amcatnloFXFX_pythia8/RunIIFall17MiniAODv2-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/MINIAODSIM'
config.Data.splitting = 'Automatic'
#config.Data.lumiMask = 'Cert_314472-325175_13TeV_17SeptEarlyReReco2018ABC_PromptEraD_Collisions18_JSON.txt'
#config.Data.unitsPerJob = 100000
config.Data.totalUnits = -1 # -1 to process all units 
config.Data.publication = False

# Output files will be stored in config.Site.storageSite at directory:   
# <config.Data.outLFNDirBase>/<config.Data.outputPrimaryDataset>/<config.Data.outputDatasetTag>/

#config.Site.storageSite = 'T3_US_FNALLPC' 
config.Site.ignoreGlobalBlacklist = True
config.Site.storageSite = 'T3_US_CMU'
config.Data.outLFNDirBase = '/store/user/acrobert/egiddata/'+date
config.Data.outputDatasetTag = config.General.requestName
