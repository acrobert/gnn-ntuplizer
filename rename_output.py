import os, sys

key = sys.argv[1]

log = True

dct = {'dp': 'DiPhotonJets_MGG-80toInf_13TeV_amcatnloFXFX_pythia8', 'pfs1': 'GJet_Pt-20to40_DoubleEMEnriched_MGG-80toInf_TuneCP5_13TeV_Pythia8',
       'pfs2': 'GJet_Pt-40toInf_DoubleEMEnriched_MGG-80toInf_TuneCP5_13TeV_Pythia8', 'pfs3': 'GJet_Pt-20toInf_DoubleEMEnriched_MGG-40to80_TuneCP5_13TeV_Pythia8'}

path = '/store/user/acrobert/egiddata/June2022/{}/pfml_June2022_ntup_{}/'.format(dct[key], key)
os.system('xrdfs root://cmsdata.phys.cmu.edu/ ls '+path+' > tmp')
path = open('tmp', 'r').read().strip('\n') + '/0000/'

if log:
    path += 'log/'
print(path)

os.system('xrdfs root://cmsdata.phys.cmu.edu/ ls '+path+' > tmp')
outlist = open('tmp', 'r').read().split('\n')

for out in outlist:
    print(out)
#exit()

to_change = []
rmv = []
for out in outlist:
    pathlist = out.split('/')
    if pathlist[-1] != '' and pathlist[-1] != 'log':
        if '-' in pathlist[-1]:
            continue
        else:
            to_change.append(out)
    else:
        rmv.append(out)
        
for out in rmv:
    print 'removing', out
    outlist.remove(out)

str_out_1 = 'output_1-{}.root'
str_log_1 = 'cmsRun_1-{}.log.tar.gz'
if log:
    str_1 = str_log_1
else:
    str_1 = str_out_1
to_fill = []
for i in range(len(outlist)):
    finpath = path + str_1.format(i+1)
    if finpath not in outlist:
        to_fill.append(finpath)

print 'same len:', len(to_change), len(to_fill)

str_out_2 = 'output_{}.root'
str_log_2 = 'cmsRun_{}.log.tar.gz'
if log:
    str_2 = str_log_2
else:
    str_2 = str_out_2

j = 0
for i in range(len(outlist)):
    outpath = path + str_2.format(i)
    if outpath in to_change:
        print('xrdfs root://cmsdata.phys.cmu.edu/ mv {} {}'.format(outpath, to_fill[j]))
        if len(sys.argv) > 2 and sys.argv[2] == 'do':
            os.system('xrdfs root://cmsdata.phys.cmu.edu/ mv {} {}'.format(outpath, to_fill[j]))
        j += 1
